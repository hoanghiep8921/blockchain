import { BigInt,log } from "@graphprotocol/graph-ts"
import { Transfer} from "../../generated/KnowOrigin/KnowOriginV2"
import { TokenV2 } from "../../generated/schema"


const GENESIS_ADDRESS = '0x0000000000000000000000000000000000000000';

export function handleTransfer(event: Transfer): void {
    let tokenId = event.params._tokenId;
    let sender = event.params._from.toHex();
    let receiver = event.params._to.toHex();

    log.info('Transfer knoworigin (v2), user_from: {}, user_to: {}, token_id: {}', [sender, receiver, tokenId.toString()]);

    let token = TokenV2.load(tokenId.toString());

    if (token == null) {
        token = new TokenV2(tokenId.toString());
        token.owner = receiver;
        //token.uri = SuperRareV1.bind(event.address).try_tokenURI(tokenId).value;
        token.removed = false;

    } else {
        if (receiver == GENESIS_ADDRESS) {
            token.removed = true;
        } else {
            token.owner = receiver;
        }
    }

    token.save();
}
